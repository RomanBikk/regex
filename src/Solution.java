import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Solution {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Pattern pattern = Pattern.compile("[a-zA-Z]{1}[a-zA-Z\\u002D\\d\\u002E\\u005F]+@([a-z]+\\u002E){1,2}((net)|(com)|(ru)|(by)|(org))");
        Matcher matcher = pattern.matcher(sc.nextLine());
        if(matcher.matches()){
            System.out.println("True");
        }
        else {
            System.out.println("False");
        }


    }


}
